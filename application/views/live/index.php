<!DOCTYPE html>
<html>
    <head>
        <title>Line Styles</title>
        <?php $this->load->view('live/script_head') ?>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <?php $this->load->view('live/style_head') ?>
    </head>

    <body>
        <div id="preloaders" class="preloader"></div>
        <div class="container my-5">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">Plant Viewer</div>
                        <div class="card-body">
                            <div class="title h5">1168 Waterbom Bali</div>
                            <hr>
                            <div class="row">
                                <div class="col-2">
                                    <p class="" style="font-size: 12px">Status</p>
                                    <div class="status-circle-active"></div>
                                    <p class="text-secondary" style="margin-left:1.1rem">NORM</p>
                                </div>
                                <div class="col-3">
                                    <p class="" style="font-size: 12px">Power Right Now</p>
                                    <div class="d-flex">
                                    <h4 id="power_value"> - </h4> <span class="text-secondary" style="font-size: 11px">/156 kW</span>
                                    <!-- <canvas id="power-now"></canvas> -->
                                    </div>
                                    <p style="font-size: 11px" class="text-secondary">Currently generating <i id="power_percent"></i>%  of plant capacity :  of <i id="power_value2"></i> kW of 156kW</p>
                                </div>
                                <div class="col-6 col-md-4">
                                    <p class="  " style="font-size: 12px">Energy <span class="text-info">Generation</span></p>
                                    <div class="content">
                                        <ul class="items">
                                            <li class="item">
                                                <div data-bind="label" class="title">TODAY</div>
                                                <div data-bind="value" class="generation h6 text-info value"><strong id="energy_today">-</strong></div>
                                                <div data-bind="units" class="units">kWh</div>
                                            </li>
                                            <li class="item">
                                                <div data-bind="label" class="title">LIFETIME</div>
                                                <div data-bind="value" class="generation h6 text-info value"><strong id="energy_lifetime">-</strong></div>
                                                <div data-bind="units" class="units">MWh</div>
                                            </li>
                                        </ul>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 mt-3">
                    <div class="card">
                        <div class="card-header">
                            Power
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <!-- <div class="col-6">
                                    <div class="buttons">
                                        <a id="1d" date-id="1" onclick="powerDate(this)" data class="btn btn-success">1D</a>
                                        <a id="7d" date-id="7" onclick="powerDate(this)" class="btn btn-outline-secondary btn-sm">7D</a>
                                        <a id="30D" date-id="30" onclick="powerDate(this)" class="btn btn-outline-secondary btn-sm">30D</a>
                                        <a id="12D" date-id="12" onclick="powerDate(this)" class="btn btn-outline-secondary btn-sm">12M</a>
                                    </div>
                                </div>
                                <div class="col-6 text-right">
                                    <div class="buttons">
                                        <a href="" class="btn btn-outline-secondary btn-sm"><</a>
                                        <span class="text-secondary" style="font-size: 11px">Sep 17, 2020 - Sep 17, 2020</span>
                                        <a href="" class="btn btn-outline-secondary btn-sm">></a>
                                    </div>
                                </div> -->
                                <div class="col-10" style="heigth: 200px">
                                    <canvas id="canvas"></canvas>
                                </div>
                                <div class="col-2 bg-secondary text-white my-3">
                                    <h5 class="text-center mt-3">Information</h5>
                                    <p style="font-size: 11px">
                                        <strong>Time Zone</strong> : Asia/Makassar <br>
                                        <strong>Location</strong> : 1168 Waterbom Bali <br>
                                        <strong>W</strong> : Watt <br>
                                        <strong>kW</strong>: Kilowatt <br>
                                        <strong>kWh</strong> : Kilowatt-hour <br>
                                        <strong>MWh</strong> : MegaWatt-hour <br>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 mt-3">
                    <div class="card">
                        <div class="card-header">Environmental Benefits</div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-4">
                                    <p>Environmental Equivalents
                                    <br>
                                    <span style="font-size:11px">Achieved by use of renewable energy</span>
                                    </p>
                                    <ul style="padding: 7px">
  
                                    <li class="d-flex">
                                        <div class="icon"><img src="https://easyview.auroravision.net/easyview/images/icons/equivalents/tv-icon.png"></div>
                                        <div class="ml-2">
                                            <span class="message">The energy to operate a TV for <strong>139,425</strong> days</span>
                                        </div>
                                    </li>
                                    
                                    <li class="d-flex">
                                        <div class="icon"><img src="https://easyview.auroravision.net/easyview/images/icons/equivalents/car-icon.png"></div>
                                        <div class="ml-2">
                                            <span class="message">The pollution an <i>average</i> passenger car emits over <strong>273.22</strong> years</span>
                                        </div>
                                    </li>
                                    
                                    <li class="d-flex">
                                        <div class="icon"><img src="https://easyview.auroravision.net/easyview/images/icons/equivalents/monitor-icon.png"></div>
                                        <div class="ml-2">
                                            <span class="message">The energy to power <strong>3,703.59</strong> computers for 1 year</span>
                                        </div>
                                    </li>
                                    
                                    </ul>
                                </div>
                                <div class="col-4">
                                    <p>Greenhouse Gases
                                        <br>
                                        <span style="font-size:11px">Greenhouse gases avoided by use of renewable energy</span>
                                    </p>
                                    <ul style="padding: 7px">
                                        <li>
                                            <p>Carbon Dioxide</p>
                                            <div class="d-flex">
                                                <div class="text-secondary h5">CO<sub>2</sub></div>
                                                <div class="ml-2">
                                                    <span class="value">2,732,204.60</span>
                                                    <span class="units">lb</span>
                                                </div>
                                            </div>
                                        </li>
                                        
                                        <li>
                                            <p>Nitrogen Oxide</p>
                                            <div class="d-flex">
                                                <div class="text-secondary h5">NO<sub>x</sub></div>
                                                <div class="ml-2">
                                                    <span class="value">2,020.70</span>
                                                    <span class="units">lb</span>
                                                </div>
                                            </div>
                                        </li>
                                        
                                        <li>
                                            <p>Sulfur Dioxide</p>
                                            <div class="d-flex">
                                                <div class="text-secondary h5">SO<sub>2</sub></div>
                                                <div class="ml-2">
                                                    <span class="value">31.90</span>
                                                    <span class="units">lb</span>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-4">
                                    <p>Carbon Offset
                                        <br>
                                        <span style="font-size:11px"><strong>1,239.31</strong> metric tons</span>
                                    </p>
                                    <img src="https://easyview.auroravision.net/easyview/images/icons/equivalents/trees.png">
                                    <p style="font-size:12px" class="text-secondary">
                                    You have offset the equivalent of: <br>
                                    <strong>264.20</strong> ac
                                    <hr>
                                    <span style="font-size: 12px">Typically one acre of pine forest will offset the equivalent of 4.69 metric tons of C02</span>
                                    </p>
                                </div>
                                <div class="col-12">
                                    <hr>
                                    <div class="row">
                                        <div class="col-6">
                                            <p class="text-secondary" style="font-size: 12px">Jl. Kartika Plaza Tuban, Kuta, BA  80361</p>
                                        </div>
                                        <div class="col-6 text-right">
                                            <p class="text-secondary" style="font-size: 12px">Install Date: 2018-09-26</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.min.js"></script>

        <?php $this->load->view('live/script') ?>
    </body>
</html>
