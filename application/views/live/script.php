<script>
    // const url = 'https://easyview.auroravision.net/easyview/services/gmi/summary.json?eids=16287344&tz=Asia%2FMakassar&start='+ Date('Ymd') +'&end='+ Date('Ymd') +'4&range=1D&fields=GenerationPower&binSize=Min15&bins=true&v=2.1.55&_=1604353472457';
    const url = "<?=base_url() ?>" + 'index.php/live/summary/';
    
    async function grafik (config) {
        var ctx = document.getElementById("canvas").getContext("2d");
        window.myLine = new Chart(ctx, config);
    }

    async function doughnut (config) {
        var ctx = document.getElementById('power-now').getContext('2d');
        window.myDoughnut = new Chart(ctx, config);
    }


    async function getDataSummary () {
        $.get(url, function(data){
            var time = [];
            var values = [];
            obj = JSON.parse(data)
            obj = JSON.parse(obj.summary)
            
            for (let a = 0; a < obj.fields[1].values.length; a++) {
                let str = obj.fields[1].values[a].startLabel
                time = time.concat( str.slice(8, 10) + ":" + str.slice(10, 12) ) ;
                values = values.concat(  parseInt(obj.fields[1].values[a].value) );
            }
            var config_grafik = {
                type: "line",
                data: {
                    labels: time,
                    datasets: [
                        {
                            label: "Power",
                            backgroundColor: window.chartColors.red,
                            borderColor: window.chartColors.red,
                            data: values,
                            fill: true,
                        },
                    ],
                },
                options: {
                    responsive: true,
                    title: {
                        display: true,
                        text: obj.fields[0].entityName,
                    },
                    tooltips: {
                        mode: "index",
                        intersect: false,
                    },
                    hover: {
                        mode: "nearest",
                        intersect: true,
                    },
                    scales: {
                        xAxes: [
                            {
                                display: true,
                                scaleLabel: {
                                    display: true,
                                    labelString: "Time",
                                },
                            },
                        ],
                        yAxes: [
                            {
                                display: true,
                                scaleLabel: {
                                    display: true,
                                    labelString: "W",
                                },
                            },
                        ],
                    },
                },
            };

            $('#power_value').html((parseFloat(obj.fields[1].values[obj.fields[1].values.length - 1].value) / 1000).toFixed(2));
            $('#power_value2').html((parseFloat(obj.fields[1].values[obj.fields[1].values.length - 1].value) / 1000).toFixed(2));
            $('#power_percent').html(parseFloat( (obj.fields[1].values[obj.fields[1].values.length - 1].value / 156000 ) * 100).toFixed(2));

            // var randomScalingFactor = function() {
            //     return Math.round(Math.random() * 100);
            // };

            // var config_pie = {
            //     type: 'doughnut',
            //     data: {
            //         datasets: [{
            //             data: [
            //                 randomScalingFactor(),
            //                 randomScalingFactor(),
            //             ],
            //             backgroundColor: [
            //                 'rgb(195 191 192)',
            //                 'rgb(235 95 131)',
            //             ],
            //             label: "used",
            //         }]
            //     },
            //     options: {
            //         responsive: true,
            //         legend: {
            //             position: 'top',
            //         },
            //         title: {
            //             display: false,
            //             text: ''
            //         },
            //         animation: {
            //             animateScale: true,
            //             animateRotate: true
            //         }
            //     }
            // };

            grafik(config_grafik);
            // doughnut(config_pie);
        });
    }

    async function getDataPlant() {
        $.get(url, function(data){
            
            obj = JSON.parse(data);
            obj = JSON.parse(obj.plant);
            
            let values = obj.fields;

            // $('#power_now').html(values[0].value + values[0].units);
            $('#energy_today').html((parseFloat(values[1].value) / 10).toFixed(2));
            $('#energy_lifetime').html((parseFloat(values[4].value) /10).toFixed(2));
            
        });
    }

    async function getApi() {
        $('#preloaders').fadeIn();
        getDataSummary();
        getDataPlant();
        $('#preloaders').fadeOut(5000);
    }

    $(document).ready(()=>{
        getApi();
        setInterval(()=>{
            getDataSummary();
            getDataPlant();
        }, (1000 * 60 * 15));
    });


</script>