<style>
    canvas {
        -moz-user-select: none;
        -webkit-user-select: none;
        -ms-user-select: none;
    }
    .status-circle-active {
        width: 50px;
        height: 50px;
        background: rgb(160,255,106);
        background: radial-gradient(circle, rgba(160,255,106,1) 0%, rgba(31,164,13,1) 61%, rgba(40,102,0,1) 100%);
        border-radius: 50%;
        margin-left: 1rem;
    }
    .span{
        font-size: 11px;
    }
    .status-circle-not-active {
        width: 50px;
        height: 50px;
        background: rgb(245,173,173);
        background: radial-gradient(circle, rgba(245,173,173,1) 0%, rgba(212,34,34,1) 76%, rgba(181,3,3,1) 100%);
        border-radius: 50%;
        margin-left: 1rem;
    }
    .content {
        margin-left: 1rem;
        font-size: .8em;
        margin-left: 5px;
        margin-right: 5px;
    }
    .content .items {
        list-style-type: none;
        margin: 0;
        padding: 0;
        display: inline-block;
    }
    .content .items li {
        display: inline-block;
        width: auto;
        border-right: 1px dotted #CCC;
        padding: 5px;
        padding-left: 10px;
        padding-right: 10px;
    }
    .content .title {
        margin-bottom: 5px;
    }
    #energy ul li .generation {
        padding-bottom: 0;
    }
    #energy ul li .generation, #energy ul li .usage {
        margin-bottom: 0;
    }
    #energy .generation {
        color: #00bad2;
    }
    .message{
        color: #6c757d!important;
        font-size: 12px;
    }
    li{
        list-style: none !important;
    }
    .value {
        font-weight: bold;
    }
    .units {
        font-weight: bold;
        color: #6c757d!important;
    }
    .preloader {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('<?= base_url() ?>/assets/logo/loader.svg') 50% 50% no-repeat rgb(249,249,249);
        opacity: 0.8;
        
    }
</style>