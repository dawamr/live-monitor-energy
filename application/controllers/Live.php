<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Live extends CI_Controller {
	public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
    }

	public function index()
	{
	
		$this->load->view('live/index');
	}

	public function data_summary()
	{
		$date_now = date('Ymdhis');
		$date_end = date('Ymdhis', strtotime("+1 day"));
		
		$data['summary'] = $this->curl("https://easyview.auroravision.net/easyview/services/gmi/summary.json?eids=16287344&tz=Asia%2FMakassar&start=". date('Ymd') ."&end=". date('Ymdhis') ."&range=1D&fields=GenerationPower&binSize=Min15&bins=true&v=2.1.55&_=1604353472457");
		$data['plant'] = $this->curl("https://easyview.auroravision.net/easyview/services/gmi/summary/PlantEnergy.json?eids=16287344&tz=Asia%2FMakassar&nDays=0%2C6%2C29&now=false&dateLabel=yyyy-MM-dd+HH%3Amm%3Ass+zzz&locale=en&fields=GenerationEnergy&v=2.1.55");
		// $data['plant_energy'] = $this->curl("https://easyview.auroravision.net/easyview/services/gmi/summary.json?eids=16287344&tz=Asia%2FMakassar&start=".$date_now."4&end=".$date_end."&range=1D&fields=GenerationPower&binSize=Min15&bins=true&v=2.1.55&_=1604485740693	");

		// mengubah JSON menjadi array
		echo json_encode($data);
	}

	function curl($url){
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt($ch, CURLOPT_USERPWD, "terregrasolar" . ":" . "Solar!18");
		$output = curl_exec($ch); 
		curl_close($ch);      
		return $output;
	}
}
